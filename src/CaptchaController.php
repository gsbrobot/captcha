<?php

namespace GoldStandard\Captcha;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CaptchaController extends Controller
{

    /**
     * @return |null
     */
    public function get()
    {
        $client = new CaptchaClient();

        return $client->getChallenge();
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse|void
     */
    public function check(Request $request)
    {
        $client = new CaptchaClient();

        return $client->checkChallenge($request);
    }
}
