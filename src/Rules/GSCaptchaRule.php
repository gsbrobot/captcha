<?php

namespace GoldStandard\Captcha\Rules;

use Carbon\Carbon;
use GoldStandard\Captcha\CaptchaCryptor;
use Illuminate\Contracts\Validation\Rule;
use GuzzleHttp\Client;

class GSCaptchaRule implements Rule
{
    protected $config;
    private $expired;

    /**
     * Create a new rule instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->config = json_decode(json_encode(config()->get('gscaptcha', [])), false);
        $this->expired = false;
    }

    /**
     * Determine if the validation rule passes.
     *
     * @param  string  $attribute
     * @param  mixed  $value
     * @return bool
     */
    public function passes($attribute, $value)
    {
        if ( !config()->get('gscaptcha.active') ){ return true; }
        try {
            if(!empty($value)) {
                if(!request()->filled('gs-captcha-verification')) {
                    return false;
                }
                if(request()->filled('gs-captcha-expiration')) {
                    $cryptor = new CaptchaCryptor($this->config->apikey);
                    $expiration = request()->get('gs-captcha-expiration');
                    if ( ($stamp = $cryptor->decrypt($expiration)) && ( is_numeric($stamp) && (int)$stamp == $stamp ) ) {
                        if (now() > Carbon::create($stamp)) {
                            $this->expired = true;
                            return false;
                        }
                    }
                }
                $data = [
                    'reference' => $value,
                    'verification'=> request()->get('gs-captcha-verification')
                ];
                $client = new Client();
                $api = $client->request(
                    'POST',
                    $this->config->serverRoutes->captchaBaseUri . $this->config->serverRoutes->captchaUseUrl,
                    [
                        'headers' => [
                            'apikey' => $this->config->apikey,
                            'secret' => $this->config->secret,

                        ],
                        'timeout' => ($this->config->timeout ?? 3),
                        'form_params' => $data
                    ]
                );

                $content = json_decode($api->getBody()->getContents());
                return  (is_object($content) && !empty($content->status)) ;

            } else {
                return false;
            }
        }
        catch(\Exception $e) {
            return false;
        }
    }

    /**
     * Get the validation error message.
     *
     * @return string
     */
    public function message()
    {
        if($this->expired) {
            return 'Captcha expired.';
        }
        return 'Captcha not matching .';
    }
}
