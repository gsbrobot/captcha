<?php
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => 'web'], function() {
    Route::get('/captcha/get', 'GoldStandard\Captcha\CaptchaController@get');
    Route::post('/captcha/check', 'GoldStandard\Captcha\CaptchaController@check');
});
