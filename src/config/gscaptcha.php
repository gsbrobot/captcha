<?php
return [
    'active' => env('GSCAPTCHA_ACTIVE', true),
    'apikey' => env('GSCAPTCHA_APIKEY', '2VDleYerM7ovXNL5qI1k'),
    'secret' => env('GSCAPTCHA_SECRET', 'xmWkAJxZRrTde7srdPx6sfaarh40vt'),
    'timeout' => env('GSCAPTCHA_CURL_TIMEOUT', 5),
    'guzzle_retries' => env('GSCAPTCHA_CURL_RETRY', 5),

    'backRoutes' => [
        'apiBaseUri' => env('APP_URL', 'http://dev.mlm'),
        'apiGetUrl' => env('API_GET_CHALLENGE', '/captcha/get'),
        'apiCheckChallenge' => env('API_CHECK_CHALLENGE','/captcha/check'),
    ],

    'serverRoutes' => [
        'captchaServerUrl' => env('GSCAPTCHA_SERVER_URL', 'https://captcha.gsb-utils.com'),
        'captchaBaseUri' => env('GSCAPTCHA_BASE_URI', 'https://captcha.gsb-utils.com/api'),
        'captchaCheckUrl' => env('GSCAPTCHA_CHECK_URL', '/reference/check'),
        'captchaUseUrl' => env('GSCAPTCHA_USE_URL', '/reference/use'),
        'captchaGetUrl' => env('GSCAPTCHA_GET_URL', '/reference/get'),
        'captchaGetScriptUrl' => env('GSCAPTCHA_GET_SCRIPT_URL', '/loadScript/cap.js'),
    ],
];
