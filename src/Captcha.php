<?php

namespace GoldStandard\Captcha;

use Illuminate\Support\Facades\Facade;
use Illuminate\Support\Facades\Log;

class Captcha  extends Facade
{
    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View
     */
    public static function render()
    {
        if ( config()->get('gscaptcha.active') )
        {
            if (config('gscaptcha.serverRoutes.captchaBaseUri') === '' ||
                config('gscaptcha.serverRoutes.captchaGetScriptUrl') === '') {
                Log::error('Could not get server routes from config on render, they return empty string');
            }

            if (config('gscaptcha.serverRoutes.captchaBaseUri') === null ||
                config('gscaptcha.serverRoutes.captchaGetScriptUrl') === null) {
                Log::error('Could not get server routes from config on render, they return null');
            }

            $routes = config('gscaptcha.backRoutes');
            $srvPath = config('gscaptcha.serverRoutes.captchaServerUrl');

            return view('vendor.goldstandard.captcha.index', compact('routes', 'srvPath'));
        }
    }
}
