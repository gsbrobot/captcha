<?php
namespace GoldStandard\Captcha;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException as gException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;

class CaptchaClient
{
    /** @var mixed  */
    protected $config;
    /** @var object  */
    private $failed;

    /**
     * CaptchaClient constructor.
     */
    public function __construct()
    {
        $this->config = json_decode(json_encode(config()->get('gscaptcha', [])), false);
        $this->failed = (object)[
            'status' => false,
            'code' => 401,
            'message' => '',
            'reference' => '',
            'expire_at' => 0
        ];
    }

    /**
     * @param null $whatever
     * @return \Illuminate\Http\JsonResponse
     */
    private function sendResponse($whatever = null)
    {
        if(!$whatever) {
            $whatever = $this->failed;
            $whatever->message = 'Nothing sent to be responded';
        }

        return response()->json($whatever);
    }

    /**
     * @param int $count
     * @return \Illuminate\Http\JsonResponse
     */
    public function getChallenge($count = 0)
    {
        try {
            if (!empty($this->config->apikey) && !empty($this->config->serverRoutes->captchaBaseUri)) {
                $client = new Client();
                $request = $client->request(
                    'POST',
                    $this->config->serverRoutes->captchaBaseUri . $this->config->serverRoutes->captchaGetUrl,
                    [
                        'headers' => [
                            'apikey' => $this->config->apikey,
                            'secret' => $this->config->secret,
                        ],
                        'timeout' => ($this->config->timeout ?? 5)
                    ]
                );

                $content = json_decode($request->getBody()->getContents());

                return $this->sendResponse($content);
            }
            Log::error('No api key or captchaBaseUri found!');
            $this->failed->message = "No credentials defined";

            return $this->sendResponse($this->failed);
        } catch (gException $e) {
            $this->failed->message = $e->getMessage();
            $this->failed->code = 501;
            Log::alert("Exception : ", [
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
                "file" => $e->getFile()
            ]);
            if($count < $this->config->guzzle_retries) {
                usleep(500);
                $this->getChallenge(++$count);
            }
            return $this->sendResponse($this->failed);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function checkChallenge(Request $request)
    {
        try {
            $requiredKeys = ['x_coordinate_percent', 'y_coordinate_percent', 'reference'];

            if ($request->filled($requiredKeys)
                && !empty($this->config->apikey)
                && !empty($this->config->secret)
                && !empty($this->config->serverRoutes->captchaBaseUri)
            ) {
                $data = $request->only($requiredKeys);
                $data['api_key'] = $this->config->apikey;
                $client = new Client();
                $api = $client->request(
                    'POST',
                    $this->config->serverRoutes->captchaBaseUri . $this->config->serverRoutes->captchaCheckUrl,
                    [
                        'headers' => [
                            'apikey' => $this->config->apikey,
                            'secret' => $this->config->secret,

                        ],
                        'timeout' => ($this->config->timeout ?? 5),
                        'form_params' => $data
                    ]
                );

                $content = json_decode($api->getBody()->getContents());
                if ( is_object($content) && $content->status && $content->expire_at){
                    $cryptor = new CaptchaCryptor($this->config->apikey);
                    $content->expiration = $cryptor->encrypt($content->expire_at);
                }

                return $this->sendResponse($content);
            }
            $this->failed->message = "No credentials defined";

            return $this->sendResponse($this->failed);

        }
        catch(gException $e) {
            $this->failed->message = $e->getMessage();
            $this->failed->code = 501;
            Log::alert("Exception : ", [
                "message" => $e->getMessage(),
                "line" => $e->getLine(),
                "file" => $e->getFile()
            ]);

            return $this->sendResponse($this->failed);
        }
    }
}
