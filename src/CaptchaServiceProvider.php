<?php

namespace GoldStandard\Captcha;

use Illuminate\Support\ServiceProvider;

class CaptchaServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        $this->app->make('GoldStandard\Captcha\CaptchaController');
        $this->app->make('GoldStandard\Captcha\CaptchaClient');
        $this->loadViewsFrom(__DIR__.'/assets/views', 'captcha');
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        include __DIR__.'/routes.php';
        $this->publishes([
            __DIR__.'/assets/css' => public_path('vendor/goldstandard/captcha/css'),
            __DIR__.'/assets/js' => public_path('vendor/goldstandard/captcha/js'),
            __DIR__.'/assets/img' => public_path('vendor/goldstandard/captcha/img'),
            __DIR__.'/assets/views' => resource_path('views/vendor/goldstandard/captcha'),
            __DIR__.'/config'=> config_path('/')
        ], 'goldstandard/captcha');
    }

}
