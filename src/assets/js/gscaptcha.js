document.getElementById('captchaModalButton').addEventListener('click', (e) => {
    initCaptcha(false);
});

/**
 * function called on button click
 * @param reload
 * @returns {boolean}
 */
function initCaptcha(reload = true){
    if(!srvPath || !routes) {
        document.getElementById("gs-captcha").remove();
        return false;
    } else {
        handler = new Captcha('gs_captcha_canvas', 'captchaModal', 'gs-captcha-reload' , srvPath, routes);
        handler.getChallenge();
        handler.setShowModal(false);
        if(!reload){
            handler.changeStatus('loading');
            handler.setShowModal(true);
        }
    }
}

class Captcha {
    /**
     *
     * @param canvas
     * @param modalId
     * @param refreshButtonId
     * @param srvPath
     * @param routes
     */
    constructor(canvas= null, modalId = null, refreshButtonId = null , srvPath = null, routes = null) {
        this.imagePath = '/api/reference/image/';
        this.backgroundName = '/main.png';
        this.pieceName = '/piece.png';

        this.eventsRegistered = false;
        this.canvas = document.getElementById(canvas);
        this.ctx = this.canvas.getContext('2d');
        this.srvPath = srvPath;
        this.challenge;
        this.backgroundImage;
        this.pieceImage;
        this.routes = routes;
        this.isDraggable = false;
        this.requestInProgress = false;
        this.currentX = 0;
        this.currentY = 0;
        this.showModal = false;
        this.modalId = '#' + modalId;
        this.modalElement = document.getElementById(modalId);
        this.refreshButton = document.getElementById(refreshButtonId);
        this.refreshes = 0;
        this.wrongs = 0;
    }

    /**
     * check if canvas is defined
     * @returns {boolean}
     */
    haveCanvas() {
        return (typeof(this.canvas) != 'undefined' && this.canvas != null);
    }

    /**
     * check required parameters for functionality
     * srvPath = this server url
     * challenge = generated challenge
     * routes = routes for backend api
     * @returns {boolean}
     */
    failCheckParams() {
        return (!this.srvPath || !this.challenge || !this.routes);
    }

    /**
     * generate canvas
     * and get images for canvas
     * @returns {boolean}
     */
    generateCanvas() {
        if(this.failCheckParams()) {
            return false;
        }

        let gurl = this.srvPath + this.imagePath;
        this.full = {
            x: 0,
            y: 0,
            width: 0,
            height: 0
        };
        this.box = {
            x: 0,
            y: 0,
            width: 0,
            height: 0,
        };
        this.backgroundImage = new Image();
        this.backgroundImage.src = gurl + this.challenge + this.backgroundName;
        this.pieceImage = new Image();
        this.pieceImage.src = gurl + this.challenge + this.pieceName;
        this.canvasLoader(false);
        this.draw();
        this.registerEvents();

    }

    /**
     * get images sizes and store
     */
    getSizes() {
        if (this.full.width == 0 || this.full.height == 0) {
            this.full.width = this.backgroundImage.width;
            this.full.height = this.backgroundImage.height;
        }
        if (this.box.width == 0 || this.box.height == 0) {
            this.box.width = this.pieceImage.width;
            this.box.height = this.pieceImage.height;
        }
        // return;
    }

    /**
     * draw images on canvas
     * and trigger animations
     *
     */
    draw() {
        this.getSizes();
        //clear the screen, draw population
        this.canvas.setAttribute('width', this.full.width);
        this.canvas.setAttribute('height', this.full.height);
        this.ctx.clearRect(0, 0, this.canvas.width, this.canvas.height);
        this.ctx.drawImage(this.backgroundImage, 0, 0, this.full.width, this.full.height);
        this.ctx.drawImage(this.pieceImage, this.box.x, this.box.y, this.box.width, this.box.height);
        let _this = this;
        if ( this.showModal ) {
            $(this.modalId).modal('show', {backdrop: 'static', keyboard: false});
            _this.setShowModal(false);
        }
        requestAnimationFrame(()=> {
            this.draw(this);
        });
    }

    /**
     * handle start events
     * mousedown / touchstart
     * @param e
     * @param event
     */
    startEvent(e, event) {
        if (event === 'touch') {
            let bcr = e.target.getBoundingClientRect();

            let xTouch = e.targetTouches[0].clientX - bcr.x;
            let yTouch = e.targetTouches[0].clientY - bcr.y;

            if (xTouch > this.box.x && xTouch < this.box.x + this.box.width) {
                if (yTouch > this.box.y && yTouch < this.box.y + this.box.height) {
                    this.isDraggable = true;
                }
            }
        } else {
            let mouseX = e.offsetX;
            let mouseY = e.offsetY;
            if ((mouseX > this.box.x && mouseX < this.box.x + this.box.width)
                && (mouseY > this.box.y && mouseY < this.box.y + this.box.height)) {
                this.isDraggable = true;

            }
        }
    }

    /**
     * handle move events
     * mousemove / touchmove
     * @param e
     * @param event
     */
    moveEvent(e, event) {
        let mouseX = e.offsetX;
        let mouseY = e.offsetY;
        if ((mouseX > this.box.x && mouseX < this.box.x + this.box.width)
            && (mouseY > this.box.y && mouseY < this.box.y + this.box.height)) {
            this.canvas.style.cursor = 'pointer';
        } else {
            this.canvas.style.cursor = 'default';
        }

        if (this.isDraggable && !this.requestInProgress) {
            if (event == 'touch') {
                var bcr = e.target.getBoundingClientRect();
                var xTouch = e.targetTouches[0].clientX - bcr.x;
                var yTouch = e.targetTouches[0].clientY - bcr.y;

                if ((xTouch + this.box.width / 2) < this.canvas.width && xTouch > (this.box.width / 2)) {
                    this.box.x = xTouch - this.box.width / 2;
                }
                if ((yTouch + this.box.height / 2) < this.canvas.height && yTouch > (this.box.height / 2)) {
                    this.box.y = yTouch - this.box.height / 2;
                }
            } else {
                if ((e.offsetX + this.box.width / 2) < this.canvas.width && e.offsetX > (this.box.width / 2)) {
                    this.box.x = e.offsetX - this.box.width / 2;
                }
                if ((e.offsetY + this.box.height / 2) < this.canvas.height && e.offsetY > (this.box.height / 2)) {
                    this.box.y = e.offsetY - this.box.height / 2;
                }
            }
            this.currentX = this.box.x;
            this.currentY = this.box.y;
        }
    }

    /**
     * End event handler
     * mouseup / touchend
     * @param e
     * @param event
     */
    endEvent(e, event) {
        // this.canvas.style.cursor = 'default';
        if(this.isDraggable && !this.requestInProgress) {
            this.canvasLoader(true, 'loading');
            this.isDraggable = false;
            e.preventDefault();
            let token = document.head.querySelector('meta[name="csrf-token"]').content;
            let xhr = new XMLHttpRequest();
            let percentX = this.currentX * 100 / this.full.width;
            let percentY = this.currentY * 100 / this.full.height;
            let json = JSON.stringify({
                _token: token,
                'x_coordinate_percent': percentX,
                'y_coordinate_percent': percentY,
                'reference': this.challenge
            });
            let _this = this;
            xhr.onloadend = function () {

                if (xhr.status == 200 && xhr.response) {
                    let response = JSON.parse(xhr.response);

                    if ( !(typeof response == "object"
                        && (typeof response.status != "undefined")
                        && (typeof response.reference != "undefined")) ) {
                        _this.getChallenge();
                        return false;
                    }
                    _this.setRequestInProgress(true);
                    switch (response.status) {
                        case true:
                            _this.changeSvgContent('success');

                            setTimeout(function(){
                                _this.setRequestInProgress(false);
                                let inputChallenge = document.querySelector('input[name="gs-captcha-input"]');
                                if(!inputChallenge) {
                                    inputChallenge = document.createElement("input");
                                    inputChallenge.name="gs-captcha-input";
                                    inputChallenge.type="hidden";
                                    let script= document.getElementById('gs-captcha');
                                    let parentDiv = document.getElementById('gs-captcha').parentNode;
                                    parentDiv.insertBefore(inputChallenge, script);
                                }
                                setTimeout(function(){
                                    inputChallenge.value = response.reference;
                                },100);

                                let inputVerification = document.querySelector('input[name="gs-captcha-verification"]');
                                if ( typeof response.verification != "undefined" ) {
                                    if ( !inputVerification ) {
                                        inputVerification = document.createElement("input");
                                        inputVerification.name="gs-captcha-verification";
                                        inputVerification.type="hidden";
                                        let script= document.getElementById('gs-captcha');
                                        let parentDiv = document.getElementById('gs-captcha').parentNode;
                                        parentDiv.insertBefore(inputVerification, script);
                                    }
                                    setTimeout( function(){
                                        inputVerification.value = response.verification;
                                    },100 );
                                }

                                let inputExpiration = document.querySelector('input[name="gs-captcha-expiration"]');
                                if ( typeof response.expiration != "undefined" ) {
                                    if ( !inputExpiration ) {
                                        inputExpiration = document.createElement("input");
                                        inputExpiration.name="gs-captcha-expiration";
                                        inputExpiration.type="hidden";
                                        let script= document.getElementById('gs-captcha');
                                        let parentDiv = document.getElementById('gs-captcha').parentNode;
                                        parentDiv.insertBefore(inputExpiration, script);
                                    }
                                    setTimeout(function(){
                                        inputExpiration.value = response.expiration;
                                    },100);
                                }

                                let btn = document.getElementById('captchaModalButton');
                                if(btn){
                                    btn.setAttribute('disabled', true);
                                }
                                _this.closeModal('success');
                            },1500);
                            break;
                        case false:
                            _this.changeSvgContent('failed');

                            setTimeout(function(){
                                if( _this.handleWrongs() == 0) {
                                    _this.closeModal('failed');
                                    return;
                                }
                                _this.setRequestInProgress(false);
                                _this.getChallenge();
                            },1500);

                            break;
                        case 'failed':
                            //TODO: handle error response
                            _this.changeSvgContent('failed');

                            setTimeout(function(){
                                if( _this.handleWrongs() == 0) {
                                    _this.closeModal('failed');
                                    return;
                                }
                                _this.setRequestInProgress(false);
                                _this.getChallenge();
                            },1500);
                            break;
                    }
                } else if(xhr.status === 419) {
                    $('#captchaModal').modal('hide');
                    $('#captchaModalButton').html('Session expired! Please refresh the page!');
                    return false;
                } else {
                    $('#captchaModal').modal('hide');
                    $('#captchaModalButton').html('There was an error with the captcha server! Please contact the support team!');
                    return false;
                }
            };
            xhr.open('POST', this.routes.apiBaseUri + this.routes.apiCheckChallenge);
            xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            xhr.send(json);

        }

    }

    /**
     * handler for refreshes number
     * @returns {number}
     */
    handleRefreshes () {
        if ( this.refreshes >=4   ) {
            this.refreshes = 0;
        } else {
            this.refreshes++;
        }
        return this.refreshes;
    }

    /**
     *
     * @returns {number}
     */
    handleWrongs () {
        if ( this.wrongs >=4   ) {
            this.wrongs = 0;
        } else {
            this.wrongs++;
        }
        return this.wrongs;
    }

    /**
     * close modal
     * @param reason
     */
    closeModal(reason) {
        let modal = this.modalElement;
        if(modal) {
            modal.setAttribute('data-reason', reason);
            $(this.modalId).modal('hide');
        }
    }

    /**
     * register events
     * @returns {boolean}
     */
    registerEvents() {
        if(this.eventsRegistered){
            return false;
        }
        /**
         * Touch Events
         */
        // this.canvas.replaceWith(this.canvas.cloneNode(true));
        this.canvas.addEventListener('mousedown', (e) => {
            this.startEvent(e, 'mouse');
        });
        this.canvas.addEventListener('mousemove', (e) => {
            this.moveEvent(e, 'mouse');
        });
        this.canvas.addEventListener('mouseup', (e) => {
            this.endEvent(e, 'mouse');
        });

        /**
         * Mouse Events
         */
        this.canvas.addEventListener('touchstart', (e) => {
            this.startEvent(e, 'touch');
            e.preventDefault();
        });
        this.canvas.addEventListener('touchmove', (e) => {
            this.moveEvent(e, 'touch');
            e.preventDefault();
        });
        this.canvas.addEventListener('touchend', (e) => {
            this.endEvent(e, 'touch');
            e.preventDefault();
        });
        /**
         * handle refresh limit
         */
        this.refreshButton.addEventListener('click', () => {
            if(!this.requestInProgress) {
                if( this.handleRefreshes() == 0) {
                    this.closeModal('failed');
                    return;
                }
                this.canvasLoader(true);
                this.getChallenge();
            }
        });

        /**
         * actions on modal close
         * call clear events on canvas
         * button change content
         */
        $(this.modalId).on('hidden.bs.modal', (e) => {

            let reason = (e && e.currentTarget && e.currentTarget.dataset && e.currentTarget.dataset.reason) ? e.currentTarget.dataset.reason : 'initial';

            this.modalElement.replaceWith(this.modalElement.cloneNode(true));
            this.clearCanvasEvents();
            this.changeStatus(reason);
            $(this.modalId).attr('data-reason', 'initial');

        });
        this.eventsRegistered = true;
    }

    /**
     * button content change
     */
    changeStatus(toShow = 'initial', message = '') {
        let toHide = document.querySelectorAll('#captchaModalButton .status');
        let found = false;
        toHide.forEach((elem) => {
            if(elem.classList.contains(toShow)) {
                if (toShow == 'failed-custom') {
                    elem.innerHTML = message;
                }
                elem.style.display = 'flex';
                found = true;
            } else {
                elem.style.display = 'none';
            }
        });
        if (!found) {
            document.querySelector('#captchaModalButton .status.initial').style.display = 'flex';
        }
    }

    /**
     * clear events on canvas
     */
    clearCanvasEvents() {
        this.canvas.replaceWith(this.canvas.cloneNode(true));
    }

    /**
     * set challenge
     * @param challenge
     */
    setChallenge ( challenge ) {
        this.challenge = challenge;
    }

    /**
     * set requestInProgress variable
     * @param value
     */
    setRequestInProgress ( value ) {
        this.requestInProgress = value;
    }

    /**
     * set modal show/hide
     * @param value
     */
    setShowModal(value = false) {
        this.showModal = value;
    }

    /**
     * canvas loader
     * @param show
     * @param type
     */
    canvasLoader(show = false, type = 'loading') {
        if ( !show ) {
            let loader = document.getElementById('gs_captcha_overlay');
            if(loader) {
                loader.remove();
            }
            return;
        }
        let overlay = document.createElement('div');
        overlay.id = 'gs_captcha_overlay';

        overlay.style.width = this.full.width + "px";
        overlay.style.height = this.full.height + "px";
        overlay.style.position = 'absolute';
        overlay.style.zIndex = 9999;
        overlay.style.backgroundColor = "rgba(0, 0, 0, 0.5)";
        overlay.style.top = this.canvas.offsetTop + "px";
        overlay.style.left = this.canvas.offsetLeft + "px";

        let svg = document.createElement("div");
        svg.id = "gs_captcha_spinner";
        svg.style.position = 'absolute';
        svg.style.top = "50%";
        svg.style.left = "50%";
        svg.style.transform = "translate(-50%,-50%)";
        svg.innerHTML = this.getSvgContent(type);
        this.canvas.insertAdjacentElement('afterend', overlay);
        let overlayElement = document.getElementById('gs_captcha_overlay');
        overlayElement.insertAdjacentElement('beforeend',svg);
    }

    /**
     * return new content for canvas spinner
     * @param type
     * @returns {string}
     */
    getSvgContent(type = 'loading') {
        let html;
        switch (type) {
            case 'success' :
                html = '<svg class="checkmark_check" width="52" height="52" viewBox="0 0 52 52" xmlns="http://www.w3.org/2000/svg" >\n' +
                    '        <circle class="checkmark__circle__check" cx="26" cy="26" r="25" fill="none"/>\n' +
                    '        <path class="checkmark__check" fill="none" d="M14.1 27.2l7.1 7.2 16.7-16.8"/>\n' +
                    '  </svg>';
                break;
            case 'failed' :
                html = '<svg class="checkmark_fail" width="52" height="52" viewBox="0 0 52 52" xmlns="http://www.w3.org/2000/svg" >\n' +
                    '        <circle class="checkmark__circle__fail" cx="26" cy="26" r="25" fill="none"/>\n' +
                    '        <path class="checkmark__fail" fill="none" d="M 15,15 L 37,37 M 37,15 L 15,37"/>\n' +
                    '  </svg>';
                break;
            case 'loading' :
                html =  '<svg class="" width="52" height="52" viewBox="0 0 52 52" xmlns="http://www.w3.org/2000/svg" stroke="#fff">' +
                    '       <g fill="none" fill-rule="evenodd">\n' +
                    '            <g transform="translate(1 1)" stroke-width="2">\n' +
                    '                <circle stroke-opacity=".5" cx="18" cy="18" r="18"/>\n' +
                    '                <path d="M36 18c0-9.94-8.06-18-18-18">\n' +
                    '                    <animateTransform\n' +
                    '                        attributeName="transform"\n' +
                    '                        type="rotate"\n' +
                    '                        from="0 18 18"\n' +
                    '                        to="360 18 18"\n' +
                    '                        dur="1s"\n' +
                    '                        repeatCount="indefinite"/>\n' +
                    '                </path>\n' +
                    '            </g>\n' +
                    '        </g>\n' +
                    '   </svg>';
                break;
        }
        return html;
    }

    /**
     * change canvas loader content
     * @param type
     */
    changeSvgContent(type = 'loading') {
        let svg = document.getElementById('gs_captcha_spinner');
        if ( svg ) {
            svg.innerHTML = this.getSvgContent(type);
        }
    }

    /**
     * generate challenge
     */
    getChallenge() {
        if(!this.requestInProgress) {
            this.setRequestInProgress(true);
            let token = document.head.querySelector('meta[name="csrf-token"]').content;
            let xhr = new XMLHttpRequest();
            let json = JSON.stringify({
                _token: token,
            });
            let _this = this;
            xhr.onloadend = function () {
                setTimeout(function(){
                    if (xhr.status == 200 && xhr.response) {
                        try {
                            let response = JSON.parse(xhr.response);
                            if(!response.status) {
                                _this.changeStatus('failed-custom', 'There was an error with the captcha server! Please try again!');
                                return false;
                            }
                            _this.setChallenge(response.reference);
                            _this.generateCanvas();
                        } catch(e) {
                            console.log('...'); // error in the above string (in this case, yes)!
                        }
                    } else {
                        _this.changeStatus('failed-custom', 'There was an error with the captcha server! Please contact the support team!');

                        return false;
                    }
                    _this.setRequestInProgress(false);
                },500);
            };
            xhr.open('GET', this.routes.apiBaseUri + this.routes.apiGetUrl);
            xhr.setRequestHeader('Content-type', 'application/json; charset=utf-8');
            xhr.send(json);
        }
    }
}

