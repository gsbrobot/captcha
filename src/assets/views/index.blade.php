﻿<script>if(typeof($.fn.modal) === 'undefined') {document.write('<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.bundle.min.js" crossorigin="anonymous"><\/script>')}</script>
<link rel="stylesheet" href="{{asset('/vendor/goldstandard/captcha/css/gscaptcha.css?v=') . $version}}">

<button type="button" class="captcha-button" id="captchaModalButton">
    <span class="status initial">
        <div class="captcha-dot"></div>
        <div class="captcha-text">I'm not a robot</div>
        <img class="captcha-logo" src="{{ asset('vendor/goldstandard/captcha/img/emblem.png') }}" alt="emblem">
    </span>
    <span class="status loading" style="display: none">
        <svg width="24" height="24" viewBox="0 0 38 38" xmlns="http://www.w3.org/2000/svg" stroke="#fff">
            <g fill="none" fill-rule="evenodd">
                <g transform="translate(1 1)" stroke-width="2">
                    <circle stroke-opacity=".5" cx="18" cy="18" r="18"/>
                    <path d="M36 18c0-9.94-8.06-18-18-18">
                        <animateTransform
                            attributeName="transform"
                            type="rotate"
                            from="0 18 18"
                            to="360 18 18"
                            dur="1s"
                            repeatCount="indefinite"/>
                    </path>
                </g>
            </g>
        </svg>
    </span>
    <span class="status success text-white" style="display: none">
        Captcha resolved!
    </span>
    <span class="status failed text-white" style="display: none">
        Too many attempts! Retry!
    </span>
    <span class="status failed-custom text-white" style="display: none">
    </span>
</button>

<!-- Modal -->
<div class="modal fade dark-mode" id="captchaModal" tabindex="-1" role="dialog" aria-labelledby="captchaModalTitle" aria-hidden="true" data-reason="initial">
    <div class="modal-dialog modal-dialog-centered justify-content-center">
        <div class="modal-content" style="width: auto!important;">
            <div class="modal-header">
                Drag the missing piece back in it's place
            </div>
            <div class="modal-body" style="min-height: 200px;">
                <canvas id="gs_captcha_canvas"></canvas>
            </div>
            <div class="modal-footer justify-content-between">
                <a style="cursor: pointer" id="reload">
                    <svg width="21" height="21" version="1.1" id="gs-captcha-reload" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px"
                         viewBox="0 0 512 512" style="enable-background:new 0 0 512 512;" xml:space="preserve">
                    <g>
                        <g>
                            <path fill="#6F6E6D" d="M463.702,162.655L442.491,14.164c-1.744-12.174-16.707-17.233-25.459-8.481l-30.894,30.894
                                C346.411,12.612,301.309,0,254.932,0C115.464,0,3.491,109.16,0.005,248.511c-0.19,7.617,5.347,14.15,12.876,15.234l59.941,8.569
                                c8.936,1.304,17.249-5.712,17.125-15.058C88.704,165.286,162.986,90,254.932,90c22.265,0,44.267,4.526,64.6,13.183l-29.78,29.78
                                c-8.697,8.697-3.761,23.706,8.481,25.459l148.491,21.211C456.508,181.108,465.105,172.599,463.702,162.655z"/>
                        </g>
                    </g>
                        <g>
                            <g>
                                <path fill="#6F6E6D" d="M499.117,249.412l-59.897-8.555c-7.738-0.98-17.124,5.651-17.124,16.143c0,90.981-74.019,165-165,165
                            c-22.148,0-44.048-4.482-64.306-13.052l28.828-28.828c8.697-8.697,3.761-23.706-8.481-25.459L64.646,333.435
                            c-9.753-1.393-18.39,6.971-16.978,16.978l21.21,148.492c1.746,12.187,16.696,17.212,25.459,8.481l31.641-31.626
                            C165.514,499.505,210.587,512,257.096,512c138.794,0,250.752-108.618,254.897-247.28
                            C512.213,257.088,506.676,250.496,499.117,249.412z"/>
                            </g>
                        </g>
                </svg>
                </a>
                <a style="cursor: pointer" data-dismiss="modal" aria-label="Close">
                    <svg width="21" height="21" viewBox="0 0 365.696 365.696" xmlns="http://www.w3.org/2000/svg">
                        <path fill="#6F6E6D" d="m243.1875 182.859375 113.132812-113.132813c12.5-12.5 12.5-32.765624 0-45.246093l-15.082031-15.082031c-12.503906-12.503907-32.769531-12.503907-45.25 0l-113.128906 113.128906-113.132813-113.152344c-12.5-12.5-32.765624-12.5-45.246093 0l-15.105469 15.082031c-12.5 12.503907-12.5 32.769531 0 45.25l113.152344 113.152344-113.128906 113.128906c-12.503907 12.503907-12.503907 32.769531 0 45.25l15.082031 15.082031c12.5 12.5 32.765625 12.5 45.246093 0l113.132813-113.132812 113.128906 113.132812c12.503907 12.5 32.769531 12.5 45.25 0l15.082031-15.082031c12.5-12.503906 12.5-32.769531 0-45.25zm0 0"/>
                    </svg>
                </a>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <input type="hidden" name="gs-captcha-input" />
</div>
<script id="gs-captcha">
    var routes=@json($routes);
    var srvPath=@json($srvPath);
</script>
<script src="{{asset('vendor/goldstandard/captcha/js/gscaptcha.js')}}"></script>
